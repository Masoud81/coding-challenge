package io.bankbridge;

/**
 * @author Masoud Aghasi
 *
 * General refactoring steps I took:
 * 1- wrapped third-party library usages (ehcache, jackson, Unirest) inside service and utility classes
 * 2- abstracted Spark service management (initialization, routing, stopping) behind HttpServer interface
 * 3- organized http request handler classes by Action interface
 *
 * Refactoring steps that seems necessary if project expands:
 * 1- Using a Dependency Injection library for managing instantiation and Singleton objects
 * 2- Logging, monitoring
 *
 * Potential Issues:
 * - One of the mock remote APIs doesn't return a bic value which could cause inconsistency for clients in real scenario
 * - Two of the mock remote APIs doesn't return a name value which could cause inconsistency for clients in real scenario
 */
public class Main {

	public static void main(String[] args) throws Exception {
		HttpServer httpServer = new MainHttpServer();
		httpServer.initService();
	}
}