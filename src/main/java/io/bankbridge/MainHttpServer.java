package io.bankbridge;

import io.bankbridge.handler.BanksCacheBased;
import io.bankbridge.handler.BanksRemoteCalls;
import spark.Service;

import java.io.IOException;

public class MainHttpServer implements HttpServer {

    private Service sparkService;

    @Override
    public void initService() throws IOException {
        sparkService = Service.ignite();
        sparkService.port(8080);

        sparkService.get("/v1/banks/all", new BanksCacheBased());
        sparkService.get("/v2/banks/all", new BanksRemoteCalls());

        sparkService.awaitInitialization();
    }

    @Override
    public void stopService() {
        sparkService.stop();
        sparkService.awaitStop();
    }
}
