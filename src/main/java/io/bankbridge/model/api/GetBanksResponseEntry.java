package io.bankbridge.model.api;

public class GetBanksResponseEntry {

    public String id;
    public String name;

    public GetBanksResponseEntry(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
