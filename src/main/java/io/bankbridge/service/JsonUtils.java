package io.bankbridge.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.bankbridge.model.BankModelList;

import java.io.IOException;
import java.util.Map;

/**
 * This is a static utility class wrapping the jackson API
 */
public final class JsonUtils {

    private static final String BANKS_V1_CONFIGS = "banks-v1.json";
    private static final String BANKS_V2_CONFIGS = "banks-v2.json";

    private JsonUtils() {}

    public static BankModelList readBanksV1JsonFile() throws IOException {
        return new ObjectMapper().readValue(
                Thread.currentThread().getContextClassLoader().getResource(BANKS_V1_CONFIGS),
                BankModelList.class);
    }

    public static Map<String, String> readBanksV2JsonFile() throws IOException {
        return new ObjectMapper().readValue(
                Thread.currentThread().getContextClassLoader().getResource(BANKS_V2_CONFIGS),
                Map.class);
    }

    public static String objectToJsonString(Object obj) throws IOException {
        return new ObjectMapper().writeValueAsString(obj);
    }
}
