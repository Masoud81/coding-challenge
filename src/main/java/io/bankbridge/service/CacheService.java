package io.bankbridge.service;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This is a Singleton class wrapping the ehcache access
 */
public class CacheService {

    private static final CacheService instance = new CacheService();

    private static final String BANKS_CACHE_NAME = "banks";
    private static final String BANK_URLS_CACHE_NAME = "bank_urls";

    private final Cache<String, String> banksCache;
    private final Cache<String, String> bankUrlsCache;


    private CacheService() {
        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache(BANKS_CACHE_NAME, CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(String.class, String.class, ResourcePoolsBuilder.heap(10)))
                .withCache(BANK_URLS_CACHE_NAME, CacheConfigurationBuilder
                        .newCacheConfigurationBuilder(String.class, String.class, ResourcePoolsBuilder.heap(10)))
                .build();

        cacheManager.init();
        banksCache = cacheManager.getCache(BANKS_CACHE_NAME, String.class, String.class);
        bankUrlsCache = cacheManager.getCache(BANK_URLS_CACHE_NAME, String.class, String.class);
    }

    public static CacheService getInstance() {
        return instance;
    }

    public Map<String, String> getBanksCacheEntries() {
        Map<String, String> banks = new ConcurrentHashMap<>();

        banksCache.forEach(entry -> banks.put(entry.getKey(), entry.getValue()));
        return banks;
    }

    public Map<String, String> getBankUrlsCacheEntries() {
        Map<String, String> bankUrls = new ConcurrentHashMap<>();

        bankUrlsCache.forEach(entry -> bankUrls.put(entry.getKey(), entry.getValue()));
        return bankUrls;
    }

    public void putBanksCacheEntry(String key, String value) {
        banksCache.put(key, value);
    }

    public void putBankUrlsCacheEntry(String key, String value) {
        bankUrlsCache.put(key, value);
    }
}
