package io.bankbridge.service;

import kong.unirest.Unirest;

/**
 * This is a static utility class wrapping the Unirest http client API
 */
public final class RemoteCallUtils {

    private RemoteCallUtils() {}

    public static <T> T getUrlAsObject(final String url, final Class<T> returnType) {
        return Unirest.get(url)
                .asObject(returnType)
                .getBody();
    }
}
