package io.bankbridge.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.bankbridge.model.api.GetBanksResponseEntry;
import io.bankbridge.service.CacheService;
import io.bankbridge.service.JsonUtils;

import io.bankbridge.model.BankModel;
import io.bankbridge.model.BankModelList;
import spark.Request;
import spark.Response;

public class BanksCacheBased implements Action {


	public BanksCacheBased() throws IOException {

		BankModelList bankModelList = JsonUtils.readBanksV1JsonFile();
		for (BankModel bankModel : bankModelList.banks) {
			CacheService.getInstance().putBanksCacheEntry(bankModel.bic, bankModel.name);
		}
	}


	@Override
	public String handle(Request request, Response response) throws Exception {

		List<GetBanksResponseEntry> result = new ArrayList<>();
		CacheService.getInstance().getBanksCacheEntries().forEach((key, value) ->
				result.add(new GetBanksResponseEntry(key, value)));

		return JsonUtils.objectToJsonString(result);
	}
}
