package io.bankbridge.handler;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import io.bankbridge.model.BankModel;
import io.bankbridge.model.api.GetBanksResponseEntry;
import io.bankbridge.service.CacheService;
import io.bankbridge.service.JsonUtils;
import io.bankbridge.service.RemoteCallUtils;
import spark.Request;
import spark.Response;

public class BanksRemoteCalls implements Action {


	public BanksRemoteCalls() throws IOException {

		Map<String, String> bankApiUrls = JsonUtils.readBanksV2JsonFile();
		bankApiUrls.forEach((bankName, url) ->
				CacheService.getInstance().putBankUrlsCacheEntry(bankName, url));
	}


	public String handle(Request request, Response response) throws Exception {

		final List<GetBanksResponseEntry> result = new CopyOnWriteArrayList<>();

		CacheService.getInstance().getBankUrlsCacheEntries().entrySet().parallelStream().forEach(entry -> {
			final BankModel model = RemoteCallUtils.getUrlAsObject(entry.getValue(), BankModel.class);
			result.add(new GetBanksResponseEntry(model.bic, model.name));
		});

		return JsonUtils.objectToJsonString(result);
	}

}
