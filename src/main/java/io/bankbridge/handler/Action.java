package io.bankbridge.handler;

import spark.Route;

/**
 * Action Interface represents a REST request handler
 * will be attached to a url path for handling the requests to that path
 */
public interface Action extends Route {

}
