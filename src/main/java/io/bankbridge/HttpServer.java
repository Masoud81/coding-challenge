package io.bankbridge;


import java.io.IOException;

/**
 * This is a wrapper interface for Spark http service
 */
public interface HttpServer {

    void initService() throws IOException;
    void stopService();
}
