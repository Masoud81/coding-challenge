package io.bankbridge;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.junit.*;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.*;

public class MainTest {

    private static final HttpServer mainHttpServer = new MainHttpServer();
    private static final HttpServer mockRemotesHttpServer = new MockRemotesHttpServer();

    @BeforeClass
    public static void setUp() throws Exception {
        mainHttpServer.initService();
        mockRemotesHttpServer.initService();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        mainHttpServer.stopService();
        mockRemotesHttpServer.stopService();
    }


    @Test
    public void testGetBanksV1_happyResponse() {

        HttpResponse<JsonNode> jsonResponse = Unirest.get("http://localhost:8080/v1/banks/all")
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .asJson();

        assertHappyResponse(jsonResponse);
    }

    @Test
    public void testGetBanksV2_happyResponse() {

        HttpResponse<JsonNode> jsonResponse = Unirest.get("http://localhost:8080/v2/banks/all")
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .asJson();

        assertHappyResponse(jsonResponse);
    }

    @Test
    public void testGetBanks_Concurrency() {

        final AtomicInteger counter = new AtomicInteger(0);
        Runnable runnable = () -> {
            testGetBanksV1_happyResponse();
            testGetBanksV2_happyResponse();
            counter.incrementAndGet();
        };
        for(int i=0 ; i<20 ; i++) {
            new Thread(runnable).start();
        }

        while (counter.get() < 20) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Test
    public void testGetBanks_networkFail() throws Exception {
        // stop MockRemotes service
        mockRemotesHttpServer.stopService();

        // v1 should working fine
        testGetBanksV1_happyResponse();

        // v2 should respond 500 status code
        HttpResponse<JsonNode> jsonResponse = Unirest.get("http://localhost:8080/v2/banks/all")
                .header("accept", "application/json")
                .header("Content-Type", "application/json")
                .asJson();

        assertEquals(500, jsonResponse.getStatus());

        // restart mockServer
        mockRemotesHttpServer.initService();
    }


    private void assertHappyResponse(HttpResponse<JsonNode> jsonResponse) {
        assertEquals(200, jsonResponse.getStatus());
        assertNotNull(jsonResponse);
        assertEquals(3, jsonResponse.getBody().getArray().length());
        for(int i=0 ; i<3 ; i++) {
            assertTrue(jsonResponse.getBody().getArray().getJSONObject(i).has("name"));
            assertTrue(jsonResponse.getBody().getArray().getJSONObject(i).has("id"));
        }
    }
}