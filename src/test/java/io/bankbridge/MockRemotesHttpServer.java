package io.bankbridge;

import spark.Service;

import java.io.IOException;

public class MockRemotesHttpServer implements HttpServer {

    private Service sparkService;

    @Override
    public void initService() throws IOException {
        sparkService = Service.ignite();
        sparkService.port(1234);

        sparkService.get("/rbb", (request, response) -> "{\n" +
                "\"bic\":\"1234\",\n" +
                "\"countryCode\":\"GB\",\n" +
                "\"auth\":\"OAUTH\"\n" +
                "}");
        sparkService.get("/cs", (request, response) -> "{\n" +
                "\"bic\":\"5678\",\n" +
                "\"countryCode\":\"CH\",\n" +
                "\"auth\":\"OpenID\"\n" +
                "}");
        sparkService.get("/bes", (request, response) -> "{\n" +
                "\"name\":\"Banco de espiritu santo\",\n" +
                "\"countryCode\":\"PT\",\n" +
                "\"auth\":\"SSL\"\n" +
                "}");

        sparkService.awaitInitialization();
    }

    @Override
    public void stopService() {
        sparkService.stop();
        sparkService.awaitStop();
    }
}
